<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
<?php 
	session_start();
	require_once('lib/config.php'); 
?>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Project 2 - Entertainment720</title>

  
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/style.css">
  

  <script src="js/vendor/custom.modernizr.js"></script>

</head>
<body>
	<header>
		<div class="row">
			<nav class="top-bar">
			  <ul class="title-area">
			    <!-- Title Area -->
			    <li class="name">
			      <h1><a href="#">Project2</a></h1>
			    </li>
			    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
			    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
			  </ul>
			

				<section class="top-bar-section">
					<ul class="right">
						<li><a href="/">Home</a></li>
						<li><a href="/">Candidates</a></li>
						<li><a href="/">Results</a></li>
						<li class="hide-for-small"><a class="button secondary radius small" href="/">Sign In</a></li>
					</ul>
				</section>
			</nav>
		</div>
		<div class="home-login-wrapper">
			<div class="home-login">
				<div class="row">
					<p class="siteheader">Cast your vote for the next Mayor of Toon-Town!</p>
					<p class="sitedesc">What's that? Your not signed in? Well, fill in your voter ID below to get started.</p>
					<div class="large-6 centered">
						<div id="message"></div>
						<form id="loginForm" action="POST">
							<input id="login_input" type="text" placeholder="Voter ID" value="" name="ID" />
							<input  id="home-login-btn" type="submit" class="button secondary radius" name="Submit" value="Sign In"/>
						</form>
					</div>
				</div>
			</div>
		<div>
	</header>

	<!-- 
	id(int),
	ballot(int),
	voted(bool),


	//new user
	home-page -> login -> home-page -> vote -> results
									-> see candidates -> vote -> results
									-> see results

	//return user
	home-page -> login -> show results -> send admin request to remove vote

	//admin
	home-page -> login -> admin-page


	 -->