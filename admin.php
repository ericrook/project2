<?php include 'header.php'; ?>

<?php

$voters = 0;

$query = "SELECT * FROM users";
if ($result = $connection->query($query)) {
	?>
     <table>
     	<thead>
        	<th>Voter ID</th>
            <th>Reset</th>
        </thead>
    <?
	
    while ($row = $result->fetch_assoc()) {
		?>
        <tr>
        	<td><?=$row['voter_id']?></td>
			<?
            if($row["ballot"] == 1 || $row["ballot"] == 2 || $row["ballot"] == 3){
                ?>
                	
                    <td class="resetVote" id="reset<?=$row['voter_id']?>"><a title="<?=$row['voter_id']?>">Reset Vote</a></</td>
                <?
            }
			else {?>
            	<td>No Vote</td>
            <?
			}
            ?>
        </tr>
        <?
    }
    $result->free();
}
?>
<div class="large-6 columns">
<a href="#" id="resetVotes">Reset All Votes</a><br />

<a href="#" id="fillRandom">Fill All Votes</a><br />
</div>

<?php include 'footer.php'; ?>