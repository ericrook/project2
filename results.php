<?php include 'header.php'; ?>

<?php
//SET VOTE COUNTS TO ZERO
$candidate1 = 0; $candidate2 = 0; $candidate3 = 0;
$voters = 0;

$query = "SELECT ballot FROM users";
if ($result = $connection->query($query)) {
    while ($row = $result->fetch_assoc()) {
		if($row["ballot"] == 1){
			$candidate1 += 1;
		}
		else if($row["ballot"] == 2){
			$candidate2 += 1;	
		}
		else if($row["ballot"] == 3){
			$candidate3 += 1;	
		}
		if($row["ballot"] == 1 || $row["ballot"] == 2 || $row["ballot"] == 3){
			$voters += 1;	
		}
    }
	$percentage_voted = ($voters / 50) * 100;
    $result->free();
}

//The counts for each candidate are in $candidate1, $candidate2 and $candidate3 variables
//echo $candidate1 . " " . $candidate2 . " " . $candidate3;

?>

<table>
	<caption>Current Election Results (<?=$percentage_voted?>% of votes in)</caption>
	<thead>
		<tr>
			<th scope="col">Candidate #1</th>
			<th scope="col">Candidate #2</th>
			<th scope="col">Candidate #3</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?=$candidate1?></td>
			<td><?=$candidate2?></td>
			<td><?=$candidate3?></td>
		</tr>		
	</tbody>
</table>

<?php include 'footer.php'; ?>