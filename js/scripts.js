jQuery(document).ready(function() {
	var sign_in = function(e) {
		e.preventDefault();

		jQuery.ajax({
			data:jQuery(this).serialize(),
			type:'POST',
			url:'lib/signin.php',
			success:function(data) {
				if(data === 'login') {
					//window.location = '/candidates.php';
					alert('you logged in');
				} else {
					//output a message about wrong credentials
					alert('you did not log in');
				}
			},
			error:function(data) {
				//output an error message
			}
		});
	};

	jQuery(function(jQuery){
		jQuery("#login_input").mask("9999");
	});

	jQuery('#loginForm').submit(sign_in);
	
	
	jQuery('td.resetVote').on('click',function(e) {
		e.preventDefault();
		var current_reset = jQuery(this).attr('id');
		
		jQuery.ajax({
			data: 'voter_id=' + jQuery(this).find('a').attr('title'),
			method: 'post',
			dataType: 'html',
			url: 'lib/admin.php',
			success: function(data) {
				$('#'+current_reset).html(data);
				$('#'+current_reset).css("color", '#4CC417');
			},
			error: function(data) {
				console.log('Error');
			}
		});
		setTimeout(function(){
			$('#'+current_reset).html('No Vote');
			$('#'+current_reset).css("color","black");
			},3000)
	});
	jQuery('a#resetVotes').on('click',function(e) {
		e.preventDefault();
		jQuery.ajax({
			data: 'resetVotes=true',
			method: 'post',
			dataType: 'html',
			url: 'lib/admin.php',
			success: function(data) {
				window.location.reload();
			},
			error: function(data) {
				console.log('Error');
			}
		});
	});
	jQuery('a#fillRandom').on('click',function(e) {
		e.preventDefault();
		jQuery.ajax({
			data: 'fillRandom=true',
			method: 'post',
			dataType: 'html',
			url: 'lib/admin.php',
			success: function(data) {
				window.location.reload();
			},
			error: function(data) {
				console.log('Error');
			}
		});
	});
	jQuery('a.vote').on('click',function(e) {
		e.preventDefault();
		var response = jQuery(this).closest('span').attr('id');
		jQuery.ajax({
			data: 'voter_id=' + $(this).attr('data-voter_id') + '&ballot=' + $(this).attr('data-ballot'),
			method: 'post',
			dataType: 'html',
			url: 'lib/vote.php',
			success: function(data) {
				$('#' + response).html(data);
			},
			error: function(data) {
				console.log('Error');
			}
		});
	});
});