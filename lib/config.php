<?php

define(DBHOST, 'localhost');
define(DBNAME, 'project2');
define(DBUSER, 'root');
define(DBPASS, 'root');
define(DBTABLE, 'users');

/*
DATABASE STRUCTURE

-users
	-voter_id(int) //limited to four digits, ex: 1001, 0
	-ballot(int) //0 = novote, 1 = candidate_1, 2 = candidate_2, 3 = candidate_3
	-time_stamp(TIMESTAMP) //Mark insisted lol
*/

$connection = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);

if (mysqli_connect_errno($connection))
{
echo "<span class='button alert' style='color:white;font-weight:bold;'>Failed to connect to MySQL: " . mysqli_connect_error() . "</span>";
}

?>